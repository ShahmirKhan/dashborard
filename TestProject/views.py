from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
# from django.forms import NameForm
from .models import Name
import boto3
import re


def creationUser(username, password, access):
    try:
        iam_client = boto3.client('iam')
        user = iam_client.create_user(
            UserName=username
        )
    except Exception as e:
        return 0, [], e
    try:
        response = iam_client.create_access_key(
            UserName=username
        )['AccessKey']
    except Exception as e:
        iam_client.delete_user(
            UserName=username
        )
        return 0, [], e
    try:
        iam_client.create_login_profile(
            UserName=username,
            Password=password,
            PasswordResetRequired=False
        )
    except Exception as e:
        iam_client.delete_access_key(
            UserName=username,
            AccessKeyId=response['AccessKeyId']
        )
        iam_client.delete_user(
            UserName=username
        )
        return 0, [], e
    if access['s3']:
        user_add = iam_client.add_user_to_group(
            GroupName='S3AccessOnly',
            UserName=username
        )
    if access['redshift']:
        user_add = iam_client.add_user_to_group(
            GroupName='RedShiftOnly',
            UserName=username
        )
    if access['redshift'] == False and False == access['s3']:
        user_add = iam_client.add_user_to_group(
            GroupName='NoAccess',
            UserName=username
        )
    return 100, [response['AccessKeyId'], response['SecretAccessKey']], ''


@csrf_exempt
def hello(request):
    ec2 = boto3.resource('ec2')
    instanceIds = []
    for instance in ec2.instances.all():
        instanceIds.append(instance.id, instance.state)
    return render(request, "hello.html", {"instanceIds": instanceIds})

    # return HttpResponse("<b>Hello " + name +"</b>")


@csrf_exempt
def create_user(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        if request.POST.get('s3access') == 'on':
            s3 = True
        else:
            s3 = False
        if request.POST.get('redshiftaccess') == 'on':
            redshift = True
        else:
            redshift = False
        if len(password) >= 8 and len(re.findall('([A-Z])', password)) > 0 and len(
                re.findall('([a-z])', password)) > 0 and len(list(filter(lambda x: password.find(x) != -1,
                                                                         ['!', '@', '#', '$', '%', '^', '&', '*', '(',
                                                                          ')', '_', '+', '-', '=', '[', ']', '{', '}',
                                                                          '|', "'"]))) > 0:
            state, keys, exc = creationUser(username, password, {'s3': s3, 'redshift': redshift})
            if state == 100:
                return render(request, "create_user.html", {'error': -1, 'message': {'accesskey': keys[0],
                                                                                     'secretaccesskey': keys[1]}})
                # return HttpResponse("<h2>accesskey : {x}  secretaccesskey : {y}</h2>".format(x=keys[0], y=keys[1]))
        else:
            return render(request, "create_user.html", {'error': 1})
        return render(request, "create_user.html", {'error': 2, 'message': exc.split("'")[1]})
    return render(request, "create_user.html", {'error': 0})


@csrf_exempt
def add_user(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        if len(password) >= 8 and len(re.findall('([A-Z])', password)) > 0 and len(
                re.findall('([a-z])', password)) > 0 and len(list(filter(lambda x: password.find(x) != -1,
                                                                         ['!', '@', '#', '$', '%', '^', '&', '*', '(',
                                                                          ')', '_', '+', '-', '=', '[', ']', '{', '}',
                                                                          '|', "'"]))) > 0:
            state, keys, exc = creationUser(username, password)
            if state == 100:
                return render(request, "create_user.html", {'error': -1, 'message': {'accesskey': keys[0],
                                                                                     'secretaccesskey': keys[1]}})
                # return HttpResponse("<h2>accesskey : {x}  secretaccesskey : {y}</h2>".format(x=keys[0], y=keys[1]))
        else:
            return render(request, "create_user.html", {'error': 1})
        return render(request, "create_user.html", {'error': 2, 'message': exc.split("'")[1]})


def index(request):
    return HttpResponse("<h2>You have reached the homepage</h2>")
